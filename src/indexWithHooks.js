import React, { useState, useRef, useEffect, useMemo, useCallback } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import ClickOutside from './components/ClickOutside';

import Content from './components/Content';
import Dropdown from './components/Dropdown';
import Loading from './components/Loading';
import Clear from './components/Clear';
import Separator from './components/Separator';
import DropdownHandle from './components/DropdownHandle';

import {
  debounce,
  hexToRGBA,
  isEqual,
  getByPath,
  getProp,
  valueExistInSelected,
  isomorphicWindow
} from './util';
import { LIB_NAME } from './constants';

function Select(_props) {
  const props = { ...defaultProps, ..._props };
  const [dropdown, setDropdown] = useState(false);
  const [values, setValues] = useState(props.values);
  const [search, setSearch] = useState('');
  const [selectBounds, setSelectBounds] = useState({});
  const [cursor, setCursor] = useState(null);
  const [searchResults, setSearchResults] = useState(props.options);

  const [otherStateValue, setOtherStateValue] = useState({});

  const [_activeCursorItem, setActiveCursorItem] = useState(null);

  const select = useRef(null);
  const isFirstRender = useRef(true);
  const dropdownRoot = typeof document !== 'undefined' && document.createElement('div');

  useEffect(() => {
    isFirstRender.current = false;
    props.portal && props.portal.appendChild(dropdownRoot);
    isomorphicWindow().addEventListener('resize', debounce(updateSelectBounds));
    isomorphicWindow().addEventListener('scroll', debounce(onScroll));

    dropDown('close');

    if (select.current) {
      updateSelectBounds();
    }

    return () => {
      props.portal && props.portal.removeChild(dropdownRoot);
      isomorphicWindow().removeEventListener(
        'resize',
        debounce(updateSelectBounds, props.debounceDelay)
      );
      isomorphicWindow().removeEventListener('scroll', debounce(onScroll, props.debounceDelay));
    };
  }, []);

  useEffect(() => {
    if (isFirstRender.current) return () => {};
    updateSelectBounds();
    setValues(props.values);
  }, [props.values]);

  useEffect(() => {
    if (isFirstRender.current) return () => {};
    updateSelectBounds();
    if (props.closeOnSelect) dropDown('close');
    if (!isEqual(props.values, values)) props.onChange(values); //this is justified because, we call the onchange and let the parent component know about the change only if parent has different values.
    console.log("values updating...",values)
  }, [values]);
  console.log(
    'values rendering inside the component--- ',
    (values || []).map((v) => v.value).join(' - ')
  );
  useEffect(() => {
    if (isFirstRender.current) return () => {};
    setSearchResults(searchResultsFn());
    updateSelectBounds();
  }, [search]);

  useEffect(() => {
    if (isFirstRender.current) return () => {};
    updateSelectBounds();
  }, [props.multi]);

  useEffect(() => {
    if (isFirstRender.current) return () => {};
    dropdown ? props.onDropdownOpen() : onDropdownClose();
  }, [dropdown]);

  useEffect(() => {
    if (isFirstRender.current) return () => {};
    setSearchResults(props.options);
  }, [props.options]);

  function getMethods() {
    return {
      removeItem,
      dropDown,
      addItem,
      setSearch: setSearchFn,
      getInputSize,
      toggleSelectAll,
      clearAll,
      selectAll,
      searchResults,
      getSelectRef,
      isSelected,
      getSelectBounds,
      areAllSelected,
      handleKeyDown,
      activeCursorItem,
      createNew,
      sortBy,
      safeString
    };
  }

  function getState() {
    return { dropdown, values, search, selectBounds, cursor, searchResults };
  }

  const onDropdownClose = () => {
    setCursor(null);
    props.onDropdownClose();
  };

  const onScroll = () => {
    if (props.closeOnScroll) {
      dropDown('close');
    }

    updateSelectBounds();
  };

  const updateSelectBounds = () =>
    select.current && setSelectBounds(select.current.getBoundingClientRect());

  const getSelectBounds = () => selectBounds;
  const dropDown = (action = 'toggle', event, force = false) => {
    const target = (event && event.target) || (event && event.srcElement);

    if (
      props.onDropdownCloseRequest !== undefined &&
      dropdown &&
      force === false &&
      action === 'close'
    ) {
      return props.onDropdownCloseRequest({
        props: props,
        methods: getMethods(),
        state: getState(),
        close: () => dropDown('close', null, true)
      });
    }

    if (
      props.portal &&
      !props.closeOnScroll &&
      !props.closeOnSelect &&
      event &&
      target &&
      target.offsetParent &&
      target.offsetParent.classList.contains('react-dropdown-select-dropdown')
    ) {
      return; //todo:figure out what the condiditon is checking
    }

    if (props.keepOpen) {
      return setDropdown(true);
    }

    if (action === 'close' && dropdown) {
      select.current.blur();
      setDropdown(false);
      setSearch(props.clearOnBlur ? '' : search);
      setSearchResults(props.options);
      return;
    }

    if (action === 'open' && !dropdown) {
      return setDropdown(true);
    }

    if (action === 'toggle') {
      select.current.focus();
      return setDropdown((d) => !d);
    }

    return false;
  };

  const getSelectRef = () => select.current;

  const addItem = (item) => {
    if (props.multi) {
      if (valueExistInSelected(getByPath(item, props.valueField), values, props)) {
        return removeItem(null, item, false);
      }

      setValues([...values, item]);
    } else {
      console.log('adding item', item);
      setValues([item]);
      setDropdown(false);
    }

    props.clearOnSelect && setSearch('');

    return true;
  };

  const removeItem = (event, item, close = false) => {
    if (event && close) {
      event.preventDefault();
      event.stopPropagation();
      dropDown('close');
    }

    setValues((v) => {
      return v.filter(
        (values) => getByPath(values, props.valueField) !== getByPath(item, props.valueField)
      );
    });
  };

  const setSearchFn = (event) => {
    setCursor(null);

    setSearch(event.target.value);
  };

  const getInputSize = () => {
    if (search) {
      return search.length;
    }

    if (values.length > 0) {
      return props.addPlaceholder.length;
    }

    return props.placeholder.length;
  };

  const toggleSelectAll = () => {
    return values.length === 0 ? selectAll() : clearAll();
  };

  const clearAll = () => {
    props.onClearAll();
    setValues([]);
  };

  const selectAll = (valuesList = []) => {
    props.onSelectAll();
    const values =
      valuesList.length > 0 ? valuesList : props.options.filter((option) => !option.disabled);

    setValues(values);
  };

  const isSelected = (option) =>
    !!values.find(
      (value) => getByPath(value, props.valueField) === getByPath(option, props.valueField)
    );

  const areAllSelected = () =>
    values.length === props.options.filter((option) => !option.disabled).length;

  const safeString = (string) => string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

  const sortBy = () => {
    const { sortBy, options } = props;

    if (!sortBy) {
      return options;
    }

    options.sort((a, b) => {
      if (getProp(a, sortBy) < getProp(b, sortBy)) {
        return -1;
      } else if (getProp(a, sortBy) > getProp(b, sortBy)) {
        return 1;
      } else {
        return 0;
      }
    });

    return options;
  };

  const searchFn = ({ state, methods }) => {
    const regexp = new RegExp(methods.safeString(state.search), 'i');

    return methods
      .sortBy()
      .filter((item) =>
        regexp.test(getByPath(item, props.searchBy) || getByPath(item, props.valueField))
      );
  };

  const searchResultsFn = () => {
    const args = { state: getState(), props: props, methods: getMethods() };

    return props.searchFn(args) || searchFn(args);
  };

  const activeCursorItem = (activeCursorItem) => setActiveCursorItem(activeCursorItem);

  const handleKeyDown = (event) => {
    const args = {
      event,
      state: getState(),
      props: props,
      methods: getMethods(),
      setState: (obj) => {
        for (var i in obj) {
          switch (i) {
            case 'values':
              setValues(obj[i]);
              break;
            case 'dropdown':
              setDropdown(obj[i]);
              break;
            case 'search':
              setSearch(obj[i]);
              break;
            case 'selectBounds':
              setSelectBounds(obj[i]);
              break;
            case 'cursor':
              setCursor(obj[i]);
              break;
            case 'searchResults':
              setSearchResults(obj[i]);
              break;
            case 'activeCursorItem':
              setActiveCursorItem(obj[i]);
              break;
            default:
              setOtherStateValue((o) => ({ ...o, [i]: obj[i] }));
          }
        }
      }
    };

    return props.handleKeyDownFn(args) || handleKeyDownFn(args);
  };

  const handleKeyDownFn = ({ event, state, props, methods, setState }) => {
    const { cursor, searchResults } = state;
    const escape = event.key === 'Escape';
    const enter = event.key === 'Enter';
    const arrowUp = event.key === 'ArrowUp';
    const arrowDown = event.key === 'ArrowDown';
    const backspace = event.key === 'Backspace';
    const tab = event.key === 'Tab' && !event.shiftKey;
    const shiftTab = event.shiftKey && event.key === 'Tab';

    if (arrowDown && !state.dropdown) {
      event.preventDefault();
      dropDown('open');
      return setState({
        cursor: 0
      });
    }

    if ((arrowDown || (tab && state.dropdown)) && cursor === null) {
      return setState({
        cursor: 0
      });
    }

    if (arrowUp || arrowDown || (shiftTab && state.dropdown) || (tab && state.dropdown)) {
      event.preventDefault();
    }

    if (escape) {
      dropDown('close');
    }

    if (enter) {
      const currentItem = searchResults[cursor];
      if (currentItem && !currentItem.disabled) {
        if (props.create && valueExistInSelected(state.search, state.values, props)) {
          return null;
        }

        methods.addItem(currentItem);
      }
    }

    if ((arrowDown || (tab && state.dropdown)) && searchResults.length === cursor) {
      return setState({
        cursor: 0
      });
    }

    if (arrowDown || (tab && state.dropdown)) {
      setState((prevState) => ({
        cursor: prevState.cursor + 1
      }));
    }

    if ((arrowUp || (shiftTab && state.dropdown)) && cursor > 0) {
      setState((prevState) => ({
        cursor: prevState.cursor - 1
      }));
    }

    if ((arrowUp || (shiftTab && state.dropdown)) && cursor === 0) {
      setState({
        cursor: searchResults.length
      });
    }

    if (backspace && props.multi && props.backspaceDelete && getInputSize() === 0) {
      setValues((v) => v.slice(0, -1));
    }
  };

  const renderDropdown = () =>
    props.portal ? (
      ReactDOM.createPortal(
        <Dropdown props={props} state={getState()} methods={getMethods()} />,
        dropdownRoot
      )
    ) : (
      <Dropdown props={props} state={getState()} methods={getMethods()} />
    );

  const createNew = (item) => {
    const newValue = {
      [props.labelField]: item,
      [props.valueField]: item
    };

    addItem(newValue);
    props.onCreateNew(newValue);
    setSearch('');
  };

  return (
    <ClickOutside onClickOutside={(event) => dropDown('close', event)}>
      <ReactDropdownSelect
        onKeyDown={handleKeyDown}
        onClick={(event) => dropDown('open', event)}
        tabIndex={props.disabled ? '-1' : '0'}
        direction={props.direction}
        style={props.style}
        ref={select}
        disabled={props.disabled}
        className={`${LIB_NAME} ${props.className}`}
        color={props.color}
        {...props.additionalProps}>
        <Content props={props} state={getState()} methods={getMethods()} />

        {(props.name || props.required) && (
          <input
            tabIndex={-1}
            style={{ opacity: 0, width: 0, position: 'absolute' }}
            name={props.name}
            required={props.required}
            pattern={props.pattern}
            defaultValue={values.map((value) => value[props.labelField]).toString() || []}
            disabled={props.disabled}
          />
        )}

        {props.loading && <Loading props={props} />}

        {props.clearable && <Clear props={props} state={getState()} methods={getMethods()} />}

        {props.separator && <Separator props={props} state={getState()} methods={getMethods()} />}

        {props.dropdownHandle && (
          <DropdownHandle
            onClick={() => (select.current && select.current).focus()}
            props={props}
            state={getState()}
            methods={getMethods()}
          />
        )}

        {dropdown && !props.disabled && renderDropdown()}
      </ReactDropdownSelect>
    </ClickOutside>
  );
}
const ReactDropdownSelect = styled.div`
  box-sizing: border-box;
  position: relative;
  display: flex;
  border: 1px solid #ccc;
  width: 100%;
  border-radius: 2px;
  padding: 2px 5px;
  flex-direction: row;
  direction: ${({ direction }) => direction};
  align-items: center;
  cursor: pointer;
  min-height: 36px;
  ${({ disabled }) =>
    disabled ? 'cursor: not-allowed;pointer-events: none;opacity: 0.3;' : 'pointer-events: all;'}

  :hover,
  :focus-within {
    border-color: ${({ color }) => color};
  }

  :focus,
  :focus-within {
    outline: 0;
    box-shadow: 0 0 0 3px ${({ color }) => hexToRGBA(color, 0.2)};
  }

  * {
    box-sizing: border-box;
  }
`;

const defaultProps = {
  addPlaceholder: '',
  placeholder: 'Select...',
  values: [],
  options: [],
  multi: false,
  disabled: false,
  searchBy: 'label',
  sortBy: null,
  clearable: false,
  searchable: true,
  dropdownHandle: true,
  separator: false,
  keepOpen: undefined,
  noDataLabel: 'No data',
  createNewLabel: 'add {search}',
  disabledLabel: 'disabled',
  dropdownGap: 5,
  closeOnScroll: false,
  debounceDelay: 0,
  labelField: 'label',
  valueField: 'value',
  color: '#0074D9',
  keepSelectedInList: true,
  closeOnSelect: false,
  clearOnBlur: true,
  clearOnSelect: true,
  dropdownPosition: 'bottom',
  dropdownHeight: '300px',
  autoFocus: false,
  portal: null,
  create: false,
  direction: 'ltr',
  name: null,
  required: false,
  pattern: undefined,
  onChange: () => undefined,
  onDropdownOpen: () => undefined,
  onDropdownClose: () => undefined,
  onDropdownCloseRequest: undefined,
  onClearAll: () => undefined,
  onSelectAll: () => undefined,
  onCreateNew: () => undefined,
  searchFn: () => undefined,
  handleKeyDownFn: () => undefined,
  additionalProps: null,
  backspaceDelete: true
};

Select.propTypes = {
  onChange: PropTypes.func.isRequired,
  onDropdownClose: PropTypes.func,
  onDropdownCloseRequest: PropTypes.func,
  onDropdownOpen: PropTypes.func,
  onClearAll: PropTypes.func,
  onSelectAll: PropTypes.func,
  values: PropTypes.array,
  options: PropTypes.array.isRequired,
  keepOpen: PropTypes.bool,
  dropdownGap: PropTypes.number,
  multi: PropTypes.bool,
  placeholder: PropTypes.string,
  addPlaceholder: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  loading: PropTypes.bool,
  clearable: PropTypes.bool,
  searchable: PropTypes.bool,
  separator: PropTypes.bool,
  dropdownHandle: PropTypes.bool,
  searchBy: PropTypes.string,
  sortBy: PropTypes.string,
  closeOnScroll: PropTypes.bool,
  openOnTop: PropTypes.bool,
  style: PropTypes.object,
  contentRenderer: PropTypes.func,
  dropdownRenderer: PropTypes.func,
  itemRenderer: PropTypes.func,
  noDataRenderer: PropTypes.func,
  optionRenderer: PropTypes.func,
  inputRenderer: PropTypes.func,
  loadingRenderer: PropTypes.func,
  clearRenderer: PropTypes.func,
  separatorRenderer: PropTypes.func,
  dropdownHandleRenderer: PropTypes.func,
  direction: PropTypes.string,
  required: PropTypes.bool,
  pattern: PropTypes.string,
  name: PropTypes.string,
  backspaceDelete: PropTypes.bool,
  triggerChangeOnValuePropChange: PropTypes.bool
};
export default Select;
